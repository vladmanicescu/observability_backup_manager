FROM python:3.7-alpine

ENV DOCKER_APP True

COPY requirements.txt .

RUN pip install -r requirements.txt

RUN apk update

RUN apk add git

RUN git config --global --add safe.directory '*'

COPY . /

WORKDIR /

ENTRYPOINT ["python3", "dashboard_backup_manager.py"]