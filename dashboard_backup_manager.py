import requests
import configparser
import json
from git import Repo
import os

def get_config():
    data = configparser.ConfigParser()
    data.read('./config/config.ini')
    grafana_endpoint = data['grafana']['uri']
    git_uri = data['git']['git_uri']
    git_branch = data['git']['git_branch']
    try:
        grafana_proxy = data['grafana']['proxy_url']
    except KeyError:
        grafana_proxy = None
    try:
        git_proxy = data['git']['proxy_url']
    except KeyError:
        git_proxy = None
    try:
        git_folder_path = data['git']['folder_path']
        git_file_path = None
        git_json_store_path = data['git']['folder_path']
    except KeyError:
        git_file_path = data['git']['file_path']
        git_folder_path = None
        git_json_store_path = data['git']['file_path']
    return grafana_endpoint, grafana_proxy, git_uri, git_proxy,git_json_store_path,git_branch, git_file_path, git_folder_path

def get_git_data(git_url,git_branch,git_proxy):
    if not git_proxy:
        Repo.clone_from(git_url, "cloned_repo",
                        branch= git_branch)
    else:
        Repo.clone_from(git_url, "cloned_repo",
                        branch= git_branch,config=f"http.proxy={git_proxy}")

def discover_directory(remote_path):
    list_of_final_paths = []
    list_of_files = []
    list_of_uids = []
    if get_config()[6]:
        for item in  os.listdir(get_config()[6]):
            intermediate_folder_path = os.path.join(remote_path, item)
            if 'json' in intermediate_folder_path:
                list_of_final_paths.append(intermediate_folder_path)
    elif get_config()[7]:
        for item in os.listdir(get_config()[7]):
            intermediate_folder_path = os.path.join(remote_path,item)
            for file in os.listdir(intermediate_folder_path):
                if 'json' in file:
                    list_of_final_paths.append(os.path.join(intermediate_folder_path,file))
    for item in list_of_final_paths:
        list_of_files.append(os.path.splitext(os.path.split(item)[1])[0])
        with open(item,'r') as f:
            json_data = f.read()
            #print(json.loads(json_data)['uid'])
            list_of_uids.append(json.loads(json_data)['uid'])
    return list_of_final_paths , list_of_uids

def push_changes_to_remote_repo():
    repo = Repo("./cloned_repo/.git")
    repo.git.add(update=True)
    repo.index.commit("Autoamted dashboard backup")
    origin = repo.remote(name='origin')
    origin.push()

class GrafanaDiscovery:
    def __init__(self):
        self.grafana_api = f'{get_config()[0]}api/'

class GrafanaFolderDiscovery(GrafanaDiscovery):
    def __init__(self):
        GrafanaDiscovery.__init__(self)
        self.grafana_api = self.grafana_api + 'folders'

    def discoverFolders(self):
        response = requests.get(self.grafana_api,verify=False)
        self.list_of_folder_dicts = json.loads(response.text)

class GrafanaDashboardDiscovery(GrafanaDiscovery):
    def __init__(self,folderList):
        GrafanaDiscovery.__init__(self)
        self.grafanaDashboardApiList = []
        for item in folderList:
            print(item['id'])
            grafana_Dashboard_api = f'{self.grafana_api}search?folderIds={item["id"]}&querry=&starred=false'
            self.grafanaDashboardApiList.append(grafana_Dashboard_api)

    def discoverDashboards(self):
        for item in self.grafanaDashboardApiList:
            response = requests.get(item,verify=False)
            response = json.loads(response.text)
            self.dashboard_list = response

    def getDashboards(self):
        self.dashboardDictionary = {}
        for item in self.dashboard_list:
           self.dashboardDictionary[item['uid']] =  item['uri'].replace('db/','')

    def getJsonModels(self,list_of_paths,list_of_uids):
        dict_of_files_and_paths = {list_of_uids[item]:list_of_paths[item] for item in range(len(list_of_uids))}
        print(dict_of_files_and_paths)
        for item in list(self.dashboardDictionary.items()):
            if item[0] in list_of_uids:
                uri = f'{self.grafana_api}dashboards/uid/{item[0]}/'
                response = requests.get(uri,verify=False)
                response = json.loads(response.text)
                with open(dict_of_files_and_paths[item[0]],'w') as outfile:
                    outfile.write(json.dumps(response['dashboard'], sort_keys=True, indent=2))
            print(item)


def main():
    get_git_data(get_config()[2],get_config()[5],get_config()[3])
    file_lists = discover_directory(get_config()[4])
    grafana_object = GrafanaFolderDiscovery()
    grafana_object.discoverFolders()
    grafana_dashboard_object = GrafanaDashboardDiscovery(grafana_object.list_of_folder_dicts)
    grafana_dashboard_object.discoverDashboards()
    grafana_dashboard_object.getDashboards()
    grafana_dashboard_object.getJsonModels(file_lists[0],file_lists[1])
    push_changes_to_remote_repo()

if __name__ == "__main__":
    main()